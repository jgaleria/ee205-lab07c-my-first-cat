//////////////////////////////////
//
// Author: Joshua Galeria
// Date  : 2/28/2022
// Desc  : Hellow world using namepspace
//
/////////////////////////////////

#include <iostream>

using namespace std;

int main() {
   cout << "Hello World" << endl; 
   return 0;
}

