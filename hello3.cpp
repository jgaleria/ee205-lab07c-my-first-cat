/////////////////////////////////
//
// Author: Joshua Galeria
// Date  : 2/28/2022
// Desc  : Hello World using objects
//
////////////////////////////////

#include <iostream>

using namespace std;

class Cat {
public: 
   void sayHello() {
      cout << "Hello World" << endl;
   }
}; 

int main() {
   Cat myCat;
   myCat.sayHello();
}


