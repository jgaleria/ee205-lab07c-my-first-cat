////////////////////////////////
//
// Author: Joshua Galeria
// Date  : 2/28/2022 
// Desc  : Hello world using standard library std::
//
////////////////////////////////

#include <iostream>

int main() {
   std::cout <<"Hello World" << std::endl;
   return 0;
}
